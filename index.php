<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @category   Ecommistry
 * @package    Ecommistry_SitemapEnhancedPlus
 * @copyright  Copyright (c) 2018 ecommistry (http://www.ecommistry.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    Release: 1.0
 * @link       http://framework.zend.com/package/PackageName
 * @since      Class available since Release 1.0
 */


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Face Compare Example</title>
</head>
<body>
<form id="face-compare" action="faceCompare.php" method="post">
    <label for="source">Source Image URL: </label>
    <input id="source" name="source" type="text" /><br>
    <label for="target">Target Image URL: </label>
    <input id="target" name="target" type="text"><br>
    <button type="submit">Submit</button><br>
</form>
</body>
</html>

