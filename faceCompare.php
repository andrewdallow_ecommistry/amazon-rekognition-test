<?php

require 'vendor/autoload.php';
use Aws\Rekognition\RekognitionClient;

$credentials = new Aws\Credentials\Credentials(
        'AKIAJBGVUH5VQCGMQY7A',
        '9JC3jSd/vZnq0K6zEZk1hPbESLlnUpnYasA5iBDs'
);

//Get Rekognition Access
$rekognitionClient = RekognitionClient::factory(array(
    'region'	=> "us-east-1",
    'version'	=> 'latest',
    'credentials' => $credentials
));

if (isset($_POST['source'])) {
    $source = $_POST['source'];
}

if (isset($_POST['target'])) {
    $target = $_POST['target'];
}
if ($source && $target) {
    //Calling Compare Face function
    $compareFaceResults= $rekognitionClient->compareFaces([
        'SimilarityThreshold' => 90,
        'SourceImage' => [
            'Bytes' => file_get_contents($source)
        ],
        'TargetImage' => [
            'Bytes' => file_get_contents($target)
        ],
    ]);
    
    $FaceMatchesResult = $compareFaceResults['FaceMatches'];
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Face Compare Result</title>
</head>
<body>


<h1>Source</h1>
<img src="<?php echo $source; ?>">
<h1>Target</h1>
<img src="<?php echo $target; ?>">
<h1>Result:</h1>
<pre>
    <?php print_r($compareFaceResults); ?>
</pre>


</body>
</html>
